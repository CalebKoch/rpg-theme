from setuptools import setup

setup(name='rpgtheme',
      version="0.0.3",
      packages=['rpgtheme', 'rpgtheme.static', 'rpgtheme.templates', 'rpgtheme.static.icon'],
      include_package_data=True,
      zip_safe=False)
