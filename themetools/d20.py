from .shapes import Shape, Text

import math

def to_rad(degrees):
	return (math.pi / 180) * degrees

def cos(degrees):
	return math.cos(to_rad(degrees))

def sin(degrees):
	return math.sin(to_rad(degrees))

def tan(degrees):
	return math.tan(to_rad(degrees))

dihedral_angle = 138.190


class D20Renderer():
	def __init__(self, x_center, y_center, l, font_size = None):
		self.center_x = x_center
		self.center_y = y_center
		self.side_length = l
		self.font_size = font_size

		self.front_text = None
		self.top_text = None
		self.left_text = None
		self.right_text = None

		self.x_c = self.center_x
		self.x_a = self.center_x - .5 * self.side_length
		self.x_b = self.center_x + .5 * self.side_length

		delta_y_c = .5 * self.side_length / cos(30)
		delta_y_a = .5 * self.side_length / tan(60)

		self.y_c = self.center_y + delta_y_c
		self.y_a = self.center_y - delta_y_a
		self.y_b = self.center_y - delta_y_a

		scale = cos(180 - dihedral_angle)
		height = (self.y_c - self.y_a) * scale

		self.x_d = self.center_x
		self.y_d = self.y_a - height

		bc_midpoint_x = (self.x_b + self.x_c) / 2
		bc_midpoint_y = (self.y_b + self.y_c) / 2

		angle_me = 30
		delta_me_x = cos(angle_me) * height
		delta_me_y = sin(angle_me) * height
		self.x_e = bc_midpoint_x + delta_me_x
		self.y_e = bc_midpoint_y + delta_me_y

		ac_midpoint_x = (self.x_a + self.x_c) / 2
		ac_midpoint_y = (self.y_a + self.y_c) / 2

		angle_mf = 150
		delta_mf_x = cos(angle_mf) * height
		delta_mf_y = sin(angle_mf) * height
		self.x_f = ac_midpoint_x + delta_mf_x
		self.y_f = ac_midpoint_y + delta_mf_y

		scale = math.pow(cos(180 - dihedral_angle), 2)

		# I believe the outer edges sides Should be parallel.
		# Use that to determine offset from the inner points.
		delta_af_x = self.x_a - self.x_f
		height = delta_af_x / cos(30)

		delta_ag_x = cos(30) * height
		delta_ag_y = sin(30) * height

		self.x_g = self.x_a - delta_ag_x
		self.y_g = self.y_a - delta_ag_y

		self.x_h = self.x_b + delta_ag_x
		self.y_h = self.y_b - delta_ag_y

		self.x_i = self.x_c
		self.y_i = self.y_c + height

	def get_text(self):
		if self.front_text:
			avg_x = (self.x_a + self.x_b + self.x_c) / 3
			avg_y = (self.y_a + self.y_b + self.y_c) / 3
			yield Text(self.front_text, (avg_x, avg_y), self.font_size)
		if self.top_text:
			avg_x = (self.x_a + self.x_b + self.x_d) / 3
			avg_y = (self.y_a + self.y_b + self.y_d) / 3
			yield Text(self.top_text, (avg_x, avg_y), self.font_size)
		if self.left_text:
			avg_x = (self.x_a + self.x_f + self.x_c) / 3
			avg_y = (self.y_a + self.y_f + self.y_c) / 3
			yield Text(self.left_text, (avg_x, avg_y), self.font_size)
		if self.right_text:
			avg_x = (self.x_e + self.x_b + self.x_c) / 3
			avg_y = (self.y_e + self.y_b + self.y_c) / 3
			yield Text(self.right_text, (avg_x, avg_y), self.font_size)

	def get_shapes(self):
		yield Shape(
			self.x_d,self.y_d,
			self.x_h,self.y_h,
			self.x_e,self.y_e,
			self.x_i,self.y_i,
			self.x_f,self.y_f,
			self.x_g,self.y_g
		)
		yield Shape(
			self.x_d,self.y_d,
			self.x_b,self.y_b,
			self.x_e,self.y_e,
			self.x_c,self.y_c,
			self.x_f,self.y_f,
			self.x_a,self.y_a
		)
		yield Shape(
			self.x_b,self.y_b,
			self.x_c,self.y_c,
			self.x_a,self.y_a
		)

		yield Shape(self.x_g, self.y_g, self.x_a, self.y_a)
		yield Shape(self.x_h, self.y_h, self.x_b, self.y_b)
		yield Shape(self.x_i, self.y_i, self.x_c, self.y_c)
