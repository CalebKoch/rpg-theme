class Shape():
	def __init__(self, *args):
		self.points = list()
		for i in range(0, len(args), 2):
			self.points.append((args[i], args[i+1]))

class Text():
	def __init__(self, text, cords, font_size):
		self.text = text
		self.cords = cords
		self.font_size = font_size
