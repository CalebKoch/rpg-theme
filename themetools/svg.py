from themetools.shapes import Shape

class SvgRenderer():
	def __init__(self, width, height=None):
		self.width = width
		if height:
			self.height = height
		else:
			self.height = width
		self.shapes = list()
		self.texts = list()
		self.styles = None

	def start_svg(self, file):
		file.write('<?xml version="1.0"?>\n')
		file.write(f'<svg width="100%" height="100%" viewBox="0 0 {self.width} {self.height}"\n')
		# Can be useful when testing generation, but for real use, we don't want a background-color
		# file.write(f'\t\tstyle="background-color:white"\n')
		file.write(f'\t\txmlns="http://www.w3.org/2000/svg"\n')
		file.write(f'\t\txmlns:svg="http://www.w3.org/2000/svg">\n')

	def end_svg(self, file):
		file.write('</svg>')

	def write_styles(self, file):
		if self.styles:
			file.write('\t<style>\n')
			for style_class in self.styles:
				file.write(f'\t\t.{style_class} {"{"}\n')
				for style, value in self.styles[style_class].items():
					file.write(f'\t\t\t{style}: {value};\n')
				file.write('\t\t}\n')
			file.write('\t</style>\n')

	def write_shape(self, file, shape):
		if len(shape.points) == 2:
			(x1,y1) = shape.points[0]
			(x2,y2) = shape.points[1]
			file.write(f'\t<line x1="{int(x1)}" y1="{int(y1)}" x2="{int(x2)}" y2="{int(y2)}" class="dice" />\n')
		elif len(shape.points) > 2:
			file.write('\t<polygon points="')
			for (x, y) in shape.points:
				file.write(f"{int(x)},{int(y)} ")
			file.write('" class="dice" />\n')

	def write_text(self, file, text):
		(x, y) = text.cords
		font_size = ""
		if text.font_size:
			font_size = f'font-size="{text.font_size}" '
		file.write(f'\t<text x="{x}" y="{y}" {font_size}class="text">{text.text}</text>\n')

	def write(self, file):
		self.start_svg(file)
		self.write_styles(file)

		for shape in self.shapes:
			self.write_shape(file, shape)
		for text in self.texts:
			self.write_text(file, text)

		self.end_svg(file)

	def add_obj(self, obj):
		self.shapes += obj.get_shapes()
		self.texts += obj.get_text()
