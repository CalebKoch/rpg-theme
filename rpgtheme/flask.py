from os import path

from . import get_loader, STATIC_DIR

from flask import Flask, send_from_directory
from jinja2 import ChoiceLoader

def configure_themes(app: Flask):
    app.jinja_loader = ChoiceLoader([app.jinja_loader, get_loader()])

    @app.route('/theme/<path:filename>')
    def _handler(filename):
        return send_from_directory(STATIC_DIR, filename)
