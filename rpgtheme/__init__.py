from os import path

from jinja2 import PackageLoader

STATIC_DIR = path.join(path.dirname(__file__), 'static')

def get_loader():
    return PackageLoader('rpgtheme', 'templates')
