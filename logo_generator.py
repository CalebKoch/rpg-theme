from themetools import d20, svg

length = 500
favicon_stroke_width = 20 # Want slightly thicker lines on the favicon
stroke_width = 10
font_size = 100

if __name__ == "__main__":
	d20obj = d20.D20Renderer(length, length, length, font_size)

	renderer = svg.SvgRenderer(length * 2)
	renderer.styles = {
		"text": {
			"font-family": "sans-serif",
			"text-anchor": "middle",
			"fill": "stroke",
			'font-size': font_size
		},
		"dice": {
			"fill-opacity": "0",
			"stroke": "black",
			"stroke-width": favicon_stroke_width,
		}
	}
	renderer.add_obj(d20obj)

	with open('rpgtheme/static/icon/favicon.svg', mode='w') as file:
		renderer.write(file)

	logo_renderer = svg.SvgRenderer(length * 2)

	renderer.styles['dice']['stroke-width'] = stroke_width
	renderer.styles['dice']['stroke'] = "#FFFDE0"
	renderer.styles['text']['fill'] = "#FFFDE0"
	logo_renderer.styles = renderer.styles

	d20obj.front_text = 'P'
	d20obj.top_text = '20'
	d20obj.left_text = 'R'
	d20obj.right_text = 'G'
	logo_renderer.add_obj(d20obj)

	with open('rpgtheme/static/icon/logo.svg', mode='w') as file:
		logo_renderer.write(file)
